package colors

import "fmt"

type Color interface {
	Blue(string) string
	Cyan(string) string
	Green(string) string
	Magenta(string) string
	Red(string) string
	White(string) string
	Yellow(string) string
	Rainbow(string) string
}

type colorPicker struct{}

func New() Color {
	return &colorPicker{}
}

func (c colorPicker) Blue(input string) string {
	return fmt.Sprintf("\u001b[34m%s\u001b[0m", input)
}

func (c colorPicker) Cyan(input string) string {
	return fmt.Sprintf("\u001b[36m%s\u001B[0m", input)
}

func (c colorPicker) Green(input string) string {
	return fmt.Sprintf("\u001b[32m%s\u001B[0m", input)
}

func (c colorPicker) Magenta(input string) string {
	return fmt.Sprintf("\u001b[35m%s\u001B[0m", input)
}

func (c colorPicker) Red(input string) string {
	return fmt.Sprintf("\u001b[31m%s\u001B[0m", input)
}

func (c colorPicker) White(input string) string {
	return fmt.Sprintf("\u001b[37m%s\u001B[0m", input)
}

func (c colorPicker) Yellow(input string) string {
	return fmt.Sprintf("\u001b[33m%s\u001B[0m", input)
}

// Rainbow cycles through all non-white colors for the given input
func (c colorPicker) Rainbow(input string) (s string) {
	for i, let := range input {
		switch i % 6 {
		case 0:
			s += c.Blue(string(let))
		case 1:
			s += c.Cyan(string(let))
		case 2:
			s += c.Green(string(let))
		case 3:
			s += c.Magenta(string(let))
		case 4:
			s += c.Red(string(let))
		case 5:
			s += c.Yellow(string(let))
		}
	}
	return
}
