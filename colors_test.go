package colors

import (
	"fmt"
	"testing"
)

func TestAllColors(t *testing.T) {
	input := "arghhhh"
	color := New()

	tests := []struct {
		name           string
		color          Color
		expectedOutput string
	}{
		{"blue", color, fmt.Sprintf("\u001b[34m%s\u001B[0m", input)},
		{"cyan", color, fmt.Sprintf("\u001b[36m%s\u001B[0m", input)},
		{"green", color, fmt.Sprintf("\u001b[32m%s\u001B[0m", input)},
		{"magenta", color, fmt.Sprintf("\u001b[35m%s\u001B[0m", input)},
		{"red", color, fmt.Sprintf("\u001b[31m%s\u001B[0m", input)},
		{"white", color, fmt.Sprintf("\u001b[37m%s\u001B[0m", input)},
		{"yellow", color, fmt.Sprintf("\u001b[33m%s\u001B[0m", input)},
		// The rainbow test is special; skip the white color and circle back to blue
		{
			"rainbow",
			color,
			fmt.Sprintf("%s%s%s%s%s%s%s",
				color.Blue("a"),
				color.Cyan("r"),
				color.Green("g"),
				color.Magenta("h"),
				color.Red("h"),
				color.Yellow("h"),
				color.Blue("h")),
		},
	}

	for i, tt := range tests {
		var output string
		switch tt.name {
		case "blue":
			output = tt.color.Blue(input)
		case "cyan":
			output = tt.color.Cyan(input)
		case "green":
			output = tt.color.Green(input)
		case "magenta":
			output = tt.color.Magenta(input)
		case "red":
			output = tt.color.Red(input)
		case "white":
			output = tt.color.White(input)
		case "yellow":
			output = tt.color.Yellow(input)
		case "rainbow":
			output = tt.color.Rainbow(input)
		default:
			t.Errorf("tests[%d] unknown color: %s", i, tt.name)
			continue
		}
		if output != tt.expectedOutput {
			t.Errorf("tests[%d]%s - expected=%s, got=%s", i, tt.name, tt.expectedOutput, output)
		}
	}
}
