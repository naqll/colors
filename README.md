![Colors!](README-SRC/colors.png)

Simple ANSI escape coloring for Go scripts.  

```
color := colors.New()
fmt.Println(color.Green("Huzzah!")
```

Included colors:
  - Blue
  - Cyan
  - Green
  - Magenta
  - Red
  - White
  - Yellow
  - Rainbow
